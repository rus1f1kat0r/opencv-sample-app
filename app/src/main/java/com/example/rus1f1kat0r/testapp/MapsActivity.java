package com.example.rus1f1kat0r.testapp;

import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.Pair;

import com.example.rus1f1kat0r.testapp.model.BugCountInformation;
import com.example.rus1f1kat0r.testapp.ui.BugDetailsFragment;
import com.example.rus1f1kat0r.testapp.viewmodel.MapDataViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    public static final String TAG_DETAILS_DIALOG = "DetailsDialog";
    private GoogleMap mMap;
    private List<Marker> currentMarkers = new ArrayList<>();
    private MapDataViewModel mapDataViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnInfoWindowClickListener(this::showDetailedInfo);
        mapDataViewModel = ViewModelProviders.of(this).get(MapDataViewModel.class);
        Transformations.map(mapDataViewModel.getBugsLiveData(), this::toMarker)
                .observe(this, this::populate);
    }

    private void showDetailedInfo(Marker marker) {
        showDetailsFragmentIfNotExist();
        mapDataViewModel.setCurrentBugInfo((String) marker.getTag());
    }

    private void showDetailsFragmentIfNotExist() {
        BottomSheetDialogFragment details = (BottomSheetDialogFragment) getSupportFragmentManager()
                .findFragmentByTag(TAG_DETAILS_DIALOG);
        if (details == null) {
            details = new BugDetailsFragment();
            details.show(getSupportFragmentManager(), TAG_DETAILS_DIALOG);
        }
    }

    private void populate(List<Pair<MarkerOptions, BugCountInformation>> all) {
        for (Marker each : currentMarkers) {
            each.remove();
        }
        currentMarkers.clear();
        for (Pair<MarkerOptions, BugCountInformation> each : all) {
            Marker marker = mMap.addMarker(each.first);
            marker.setTag(each.second.getUuid());
            currentMarkers.add(marker);
        }
        if (all.size() >= 1){
            mMap.moveCamera(CameraUpdateFactory.newLatLng(all.get(0).first.getPosition()));
        }
    }

    private List<Pair<MarkerOptions, BugCountInformation>> toMarker(List<BugCountInformation> infos) {
        List<Pair<MarkerOptions, BugCountInformation>> result = new ArrayList<>();
        if (infos == null) {
            return result;
        }
        for (BugCountInformation each : infos) {
            LatLng latLng = new LatLng(each.getLocation().getLat(), each.getLocation().getLng());
            result.add(getMarker(each, latLng)
            );
        }
        return result;
    }

    @NonNull
    private Pair<MarkerOptions, BugCountInformation> getMarker(BugCountInformation each, LatLng latLng) {
        MarkerOptions marker = new MarkerOptions()
                .position(latLng)
                .title(each.getName())
                .snippet("Bugs detected for this photo - " + each.getCount())
                .draggable(false);
        return new Pair<>(marker, each);
    }
}
