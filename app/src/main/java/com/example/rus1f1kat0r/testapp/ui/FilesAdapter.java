package com.example.rus1f1kat0r.testapp.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.example.rus1f1kat0r.testapp.R;
import com.example.rus1f1kat0r.testapp.model.BugImage;

import java.io.File;
import java.util.List;

import static com.bumptech.glide.request.RequestOptions.centerCropTransform;
import static com.bumptech.glide.request.RequestOptions.downsampleOf;

public class FilesAdapter extends RecyclerView.Adapter<FilesAdapter.Holder> {

    private final Context context;
    private final List<BugImage> allImages;
    private final OnBugFileClickListener listener;

    public FilesAdapter(Context context, List<BugImage> allImages, OnBugFileClickListener listener) {
        this.context = context;
        this.allImages = allImages;
        this.listener = listener;
    }

    public void setAllImages(List<BugImage> allImages) {
        this.allImages.clear();
        this.allImages.addAll(allImages);
        notifyDataSetChanged();
    }

    @Override
    public FilesAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FilesAdapter.Holder(
                LayoutInflater.from(context).inflate(R.layout.bug_img_item, parent, false)) {};
    }

    @Override
    public void onBindViewHolder(FilesAdapter.Holder holder, int position) {
        BugImage bugImage = allImages.get(position);
        holder.name.setText(bugImage.getName());
        Glide.with(context)
                .asDrawable()
                .load(new File(bugImage.getUri().toString()))
                .apply(centerCropTransform())
                .apply(downsampleOf(DownsampleStrategy.CENTER_OUTSIDE))
                .into(holder.preview);

    }

    @Override
    public int getItemCount() {
        return allImages.size();
    }

    public interface OnBugFileClickListener {
        void onBugFileClick(BugImage file);
    }

    public class Holder extends RecyclerView.ViewHolder {

        private final ImageView preview;
        private final TextView name;

        Holder(View itemView) {
            super(itemView);
            preview = itemView.findViewById(R.id.preview);
            name = itemView.findViewById(R.id.name);
            itemView.setOnClickListener(view -> listener.onBugFileClick(allImages.get(getAdapterPosition())));
        }
    }
}
