package com.example.rus1f1kat0r.testapp.setup;


import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

public class LocationPermission implements SetUp {

    public static final int LOCATION_REQUEST_CODE = 25;
    private final Activity context;

    public LocationPermission(Activity context) {
        this.context = context;
    }

    @Override
    public void setUp() {
        ActivityCompat.requestPermissions(context,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                LOCATION_REQUEST_CODE);
    }

    @Override
    public boolean verify() {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }
}
