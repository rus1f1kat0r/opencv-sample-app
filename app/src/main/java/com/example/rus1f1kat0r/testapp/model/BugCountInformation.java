package com.example.rus1f1kat0r.testapp.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BugCountInformation {

    private String uuid;
    private String name;
    private Location location;
    private long time;
    private int count;
    private String description;

    public BugCountInformation() {
        this("", "", 0, 0, 0, 0, "");
    }

    public BugCountInformation(String uuid, String name, double lat, double lng, int count, long time, String description) {
        this.uuid = uuid;
        this.name = name;
        this.location = new Location(lat, lng);
        this.count = count;
        this.time = time;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public int getCount() {
        return count;
    }

    public String getCountString() {
        return String.valueOf(count);
    }

    public String getUuid() {
        return uuid;
    }

    public long getTime() {
        return time;
    }

    public String getTimeString() {
        if (time <= 0) {
            return "";
        }
        SimpleDateFormat sdf = (SimpleDateFormat) SimpleDateFormat.getDateTimeInstance();
        sdf.applyPattern("dd/MMM/yyyy hh:mm");
        return sdf.format(new Date(time));
    }

    public String getDescription() {
        return description;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public void setTimeString(String time) {
        SimpleDateFormat sdf = (SimpleDateFormat) SimpleDateFormat.getDateTimeInstance();
        sdf.applyPattern("dd/MMM/yyyy hh:mm");
        try {
            this.time = sdf.parse(time).getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setCountString(String count) {
        try {
            this.count = Integer.parseInt(count);
        } catch (Exception e) {

        }
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BugCountInformation that = (BugCountInformation) o;

        if (count != that.count) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (location != null ? !location.equals(that.location) : that.location != null) return false;
        return uuid != null ? uuid.equals(that.uuid) : that.uuid == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + count;
        result = 31 * result + (uuid != null ? uuid.hashCode() : 0);
        return result;
    }
}
