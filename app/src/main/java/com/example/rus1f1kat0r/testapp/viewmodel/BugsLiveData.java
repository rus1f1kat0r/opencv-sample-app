package com.example.rus1f1kat0r.testapp.viewmodel;

import android.arch.lifecycle.MutableLiveData;

import com.example.rus1f1kat0r.testapp.model.BugCountInformation;
import com.example.rus1f1kat0r.testapp.service.BugPhotosRepository;

import java.util.List;

public class BugsLiveData extends MutableLiveData<List<BugCountInformation>> {

    private final BugPhotosRepository repository;
    private final BugPhotosRepository.BugsInfoListener listener = new BugPhotosRepository.BugsInfoListener(this::postValue);

    public BugsLiveData(BugPhotosRepository repository) {
        this.repository = repository;
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        repository.removeOnDataListener(listener);
    }

    @Override
    protected void onActive() {
        super.onActive();
        repository.addOnDataListener(listener);
    }
}
