package com.example.rus1f1kat0r.testapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.opencv.android.Utils.bitmapToMat;
import static org.opencv.core.Core.addWeighted;
import static org.opencv.core.Core.inRange;
import static org.opencv.imgcodecs.Imgcodecs.imwrite;
import static org.opencv.imgproc.Imgproc.CV_CONTOURS_MATCH_I2;
import static org.opencv.imgproc.Imgproc.Canny;
import static org.opencv.imgproc.Imgproc.GaussianBlur;
import static org.opencv.imgproc.Imgproc.approxPolyDP;
import static org.opencv.imgproc.Imgproc.boundingRect;
import static org.opencv.imgproc.Imgproc.findContours;
import static org.opencv.imgproc.Imgproc.matchShapes;
import static org.opencv.imgproc.Imgproc.medianBlur;
import static org.opencv.imgproc.Imgproc.rectangle;

public class BugCount {

    private final Context context;

    private Mat bgr_image = new Mat();
    private Mat detected_edges = new Mat();

    private Mat filtered_image = new Mat();
    private Mat red_filtered_image = new Mat();
    private Mat red_hue_image = new Mat();

    private int lowThreshold = 50;
    private final int max_lowThreshold = 150;

    private int ratio = 3;
    private int kernel_size = 3;

    private int contour_length_low_threshold = 30;
    private int contour_length_high_threshold = 60;

    public BugCount(Context context) {
        this.context = context;
    }

    private void check_if_image_exist(Mat bgr_image) {
        if (bgr_image.empty()) {
            throw new RuntimeException("image can not be loaded");
        }
    }

    public int calculate(Uri path_image) throws FileNotFoundException {
        bgr_image = imgRead(path_image);
        // Check if the image can be loaded
        check_if_image_exist(bgr_image);

        medianBlur(bgr_image, filtered_image, 9);

        // Convert input image to HSV
        Mat hsv_image = new Mat();
        Imgproc.cvtColor(filtered_image, hsv_image, Imgproc.COLOR_BGR2HSV);

        // Threshold the HSV image, keep only the red pixels
        Mat lower_red_hue_range = new Mat();
        Mat upper_red_hue_range = new Mat();

        inRange(hsv_image, new Scalar(0, 0, 0), new Scalar(5, 100, 100), lower_red_hue_range);
        inRange(hsv_image, new Scalar(160, 0, 0), new Scalar(179, 100, 100), upper_red_hue_range);

        // Combine the above two images
        addWeighted(lower_red_hue_range, 1.0, upper_red_hue_range, 1.0, 0.0, red_hue_image);
        medianBlur(red_hue_image, red_filtered_image, 9);

        /// Canny detector
        Canny(red_filtered_image, detected_edges, new MatOfInt(lowThreshold), lowThreshold * ratio, kernel_size);

        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();

        /// Find contours
        findContours(detected_edges, contours, hierarchy, 3, 2, new Point(0, 0));

        Iterator<MatOfPoint> iterator = contours.iterator();
        while (iterator.hasNext()) {
            MatOfPoint it = iterator.next();
            if ((it.height() < contour_length_low_threshold) || (it.height() > contour_length_high_threshold))
                iterator.remove();
        }

        List<MatOfPoint2f> contours_poly = new ArrayList<>(contours.size());
        for (int i = 0; i < contours.size(); i++) {
            contours_poly.add(new MatOfPoint2f());
        }
        List<Rect> boundRect = new ArrayList<>(contours.size());

        for (int i = 0; i < contours.size(); i++) {
            MatOfPoint2f curve = new MatOfPoint2f();
            contours.get(i).convertTo(curve, CvType.CV_32FC2);
            approxPolyDP(curve, contours_poly.get(i), 3, true);
            MatOfPoint b = new MatOfPoint();
            contours_poly.get(i).convertTo(b, CvType.CV_32S);
            boundRect.add(boundingRect(b));
        }

        Mat drawing = new Mat();
        bgr_image.copyTo(drawing);

        List<MatOfPoint> contour_ref_1 = new ArrayList<>();
        Mat hierarchy_ref_1 = new Mat();

        Mat ref_image = imgRead(R.raw.bug_ref_1);
        Mat ref_filtered_image = new Mat();
        Mat ref_detected_edges = new Mat();

        GaussianBlur(ref_image, ref_filtered_image, new Size(9, 9), 0, 0);
        Canny(ref_filtered_image, ref_detected_edges, new MatOfInt(lowThreshold), lowThreshold * ratio, kernel_size);

        GaussianBlur(ref_detected_edges, ref_detected_edges, new Size(3, 3), 0, 0);
        findContours(ref_detected_edges, contour_ref_1, hierarchy_ref_1, 3, 2, new Point(0, 0));

        List<MatOfPoint> contour_ref_2 = new ArrayList<>();
        Mat hierarchy_ref_2 = new Mat();

        ref_image = imgRead(R.raw.bug_ref_2);
        ref_filtered_image = new Mat();
        ref_detected_edges = new Mat();

        GaussianBlur(ref_image, ref_filtered_image, new Size(9, 9), 0, 0);
        Canny(ref_filtered_image, ref_detected_edges, new MatOfInt(lowThreshold), lowThreshold * ratio, kernel_size);

        GaussianBlur(ref_detected_edges, ref_detected_edges, new Size(3, 3), 0, 0);
        findContours(ref_detected_edges, contour_ref_2, hierarchy_ref_2, 3, 2, new Point(0, 0));

        List<MatOfPoint> contour_ref_3 = new ArrayList<>();
        Mat hierarchy_ref_3 = new Mat();

        ref_image = imgRead(R.raw.bug_ref_3);
        ref_filtered_image = new Mat();
        ref_detected_edges = new Mat();

        GaussianBlur(ref_image, ref_filtered_image, new Size(9, 9), 0, 0);
        Canny(ref_filtered_image, ref_detected_edges, new MatOfInt(lowThreshold), lowThreshold * ratio, kernel_size);

        GaussianBlur(ref_detected_edges, ref_detected_edges, new Size(3, 3), 0, 0);
        findContours(ref_detected_edges, contour_ref_3, hierarchy_ref_3, 3, 2, new Point(0, 0));

        double match_result_1, match_result_2, match_result_3;
        int bug_count = 0;

        for (int i = 0; i < contours.size(); i++) {
            match_result_1 = matchShapes(contours.get(i), contour_ref_1.get(0), CV_CONTOURS_MATCH_I2, 0);
            match_result_2 = matchShapes(contours.get(i), contour_ref_2.get(0), CV_CONTOURS_MATCH_I2, 0);
            match_result_3 = matchShapes(contours.get(i), contour_ref_3.get(0), CV_CONTOURS_MATCH_I2, 0);

            if ((match_result_1 < 2.0) || (match_result_2 < 2.0) || (match_result_3 < 2.0)) {
                Scalar color = new Scalar(0, 0, 255);
                rectangle(drawing, boundRect.get(i).tl(), boundRect.get(i).br(), color, 2, 8, 0);

                bug_count++;
            }
        }

        try {
            String image = path_image.toString().substring(0, path_image.toString().length() - 4);
            imwrite(image + "_output_count_ " + bug_count + ".jpg", drawing);
        } catch (Throwable e) {

        }
        return bug_count;
    }

    private Mat imgRead(int id) {
        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bmpFactoryOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;

        Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), id, bmpFactoryOptions);
        Mat imageMat = new Mat();
        bitmapToMat(bmp, imageMat);
        return imageMat;
    }


    private Mat imgRead(Uri uri) throws FileNotFoundException {

        Bitmap bmp = BitmapFactory.decodeFile(uri.toString());
        if (bmp == null ) {
            throw new FileNotFoundException();
        }
        Mat imageMat = new Mat();
        bitmapToMat(bmp, imageMat);
        return imageMat;
    }
}