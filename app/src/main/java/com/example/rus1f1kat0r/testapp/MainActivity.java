package com.example.rus1f1kat0r.testapp;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.example.rus1f1kat0r.testapp.model.BugCountInformation;
import com.example.rus1f1kat0r.testapp.model.BugImage;
import com.example.rus1f1kat0r.testapp.setup.InitializationManager;
import com.example.rus1f1kat0r.testapp.setup.LocationPermission;
import com.example.rus1f1kat0r.testapp.setup.OpenCvInstalled;
import com.example.rus1f1kat0r.testapp.setup.WriteFilePermission;
import com.example.rus1f1kat0r.testapp.ui.FilesAdapter;
import com.example.rus1f1kat0r.testapp.viewmodel.BugCountingViewModel;
import com.example.rus1f1kat0r.testapp.viewmodel.MapDataViewModel;
import com.example.rus1f1kat0r.testapp.viewmodel.ViewModelFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private BugCountingViewModel bugCountingViewModel;
    private MapDataViewModel mapDataViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bugCountingViewModel = ViewModelProviders.of(this, new ViewModelFactory(getApplication()))
                .get(BugCountingViewModel.class);
        bugCountingViewModel.getBugCountLiveData().observe(this, this::setBugCountTextLabel);
        mapDataViewModel = ViewModelProviders.of(this).get(MapDataViewModel.class);
        findViewById(R.id.viewMap).setOnClickListener(v -> startActivity(new Intent(this, MapsActivity.class)));
    }

    private void setBugCountTextLabel(BugCountInformation bugInfo) {
        if (bugInfo != null) {
            TextView textView = findViewById(R.id.sample_text);
            textView.setText(String.format("bugs count - %s", bugInfo.getCount()));
            Snackbar.make(
                    findViewById(R.id.container),
                    "This Bug Information is ready to be saved ",
                    Snackbar.LENGTH_LONG)
                    .setAction("Save", v -> mapDataViewModel.addBugCountInformation(bugInfo))
                    .show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        tryToSetUp();
    }

    private void tryToSetUp() {
        InitializationManager
                .with(new WriteFilePermission(this))
                .also(new OpenCvInstalled(this, this::tryToSetUp))
                .also(new LocationPermission(this))
                .doOnSuccess(this::initBugFiles)
                .initialize();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case WriteFilePermission.WRITE_EXTERNAL_STORAGE_CODE:
            case LocationPermission.LOCATION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    tryToSetUp();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void initBugFiles() {
        RecyclerView recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new FilesAdapter(this, getSample(), bugCountingViewModel::setBugImage));
    }

    private List<BugImage> getSample() {
        List<BugImage> result = new ArrayList<>();
        File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");
        File[] files = directory.listFiles();
        if (files != null) {
            for (File each: files) {
                result.add(new BugImage(Uri.parse(each.getAbsolutePath()), each.getName()));
            }
        }
        return result;
//        return Arrays.asList(
//                new BugImage(Uri.parse("android.resource://com.example.rus1f1kat0r.testapp/raw/s20170905_085111"), "20170905_085111.jpg"),
//                new BugImage(Uri.parse("android.resource://com.example.rus1f1kat0r.testapp/raw/s20170905_085128"), "20170905_085128.jpg"),
//                new BugImage(Uri.parse("android.resource://com.example.rus1f1kat0r.testapp/raw/s20170905_085132"), "s20170905_085132.jpg"),
//                new BugImage(Uri.parse("android.resource://com.example.rus1f1kat0r.testapp/raw/s20170905_085156"), "s20170905_085156.jpg"),
//                new BugImage(Uri.parse("android.resource://com.example.rus1f1kat0r.testapp/raw/s20170905_085202"), "s20170905_085202.jpg"),
//                new BugImage(Uri.parse("android.resource://com.example.rus1f1kat0r.testapp/raw/s20170906_100523"), "s20170906_100523.jpg"),
//                new BugImage(Uri.parse("android.resource://com.example.rus1f1kat0r.testapp/raw/s20170906_101627"), "s20170906_101627.jpg"),
//                new BugImage(Uri.parse("android.resource://com.example.rus1f1kat0r.testapp/raw/s20170906_101632"), "s20170906_101632.jpg"),
//                new BugImage(Uri.parse("android.resource://com.example.rus1f1kat0r.testapp/raw/s20170906_101647"), "s20170906_101647.jpg"),
//                new BugImage(Uri.parse("android.resource://com.example.rus1f1kat0r.testapp/raw/s20170906_101729"), "s20170906_101729.jpg"),
//                new BugImage(Uri.parse("android.resource://com.example.rus1f1kat0r.testapp/raw/s20170906_101734"), "s20170906_101734.jpg"),
//                new BugImage(Uri.parse("android.resource://com.example.rus1f1kat0r.testapp/raw/s20170906_101746"), "s20170906_101746.jpg")
//        );
    }

}
