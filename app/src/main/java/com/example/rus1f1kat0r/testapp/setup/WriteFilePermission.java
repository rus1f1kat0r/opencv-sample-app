package com.example.rus1f1kat0r.testapp.setup;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

public class WriteFilePermission implements SetUp {

    public static final int WRITE_EXTERNAL_STORAGE_CODE = 24;
    private final Activity context;

    public WriteFilePermission(Activity context) {
        this.context = context;
    }

    @Override
    public void setUp() {
         ActivityCompat.requestPermissions(context,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    WRITE_EXTERNAL_STORAGE_CODE);
    }

    @Override
    public boolean verify() {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }
}
