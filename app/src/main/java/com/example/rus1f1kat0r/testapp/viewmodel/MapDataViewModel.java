package com.example.rus1f1kat0r.testapp.viewmodel;


import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;

import com.example.rus1f1kat0r.testapp.model.BugCountInformation;
import com.example.rus1f1kat0r.testapp.service.BugPhotosRepository;
import com.example.rus1f1kat0r.testapp.service.DeviceIdentification;

import java.util.List;

public class MapDataViewModel extends AndroidViewModel {

    private final BugPhotosRepository repository;
    private final BugsLiveData bugsLiveData;
    private final MutableLiveData<String> currentDetails;
    private final LiveData<BugCountInformation> bugInfo;

    public MapDataViewModel(@NonNull Application application) {
        super(application);
        repository = new BugPhotosRepository(new DeviceIdentification(application));
        bugsLiveData = new BugsLiveData(repository);
        currentDetails = new MutableLiveData<>();
        bugInfo = Transformations.switchMap(currentDetails, input -> new BugInfoDetailsLiveData(repository, input));
    }

    public LiveData<List<BugCountInformation>> getBugsLiveData() {
        return bugsLiveData;
    }

    public void addBugCountInformation(BugCountInformation info) {
        repository.save(info);
    }

    public LiveData<BugCountInformation> getCurrentBugInfo() {
        return bugInfo;
    }

    public void setCurrentBugInfo(String uuid) {
        currentDetails.setValue(uuid);
    }
}
