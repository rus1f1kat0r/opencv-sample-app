package com.example.rus1f1kat0r.testapp.viewmodel;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.util.Pair;

import com.example.rus1f1kat0r.testapp.BugCount;
import com.example.rus1f1kat0r.testapp.model.BugCountInformation;
import com.example.rus1f1kat0r.testapp.model.BugImage;

import java.io.FileNotFoundException;
import java.lang.ref.WeakReference;
import java.util.UUID;

public class BugCountingViewModel extends ViewModel {

    private final WeakReference<Context> contextRef;
    private final MutableLiveData<Pair<BugImage, Integer>> bugCountLiveData = new MutableLiveData<>();
    private final LiveData<Location> locationLiveData;
    private final MediatorLiveData<BugCountInformation> mediatorLiveData = new MediatorLiveData<>();

    BugCountingViewModel(Context context) {
        this.contextRef = new WeakReference<>(context);
        this.locationLiveData = new LocationLiveData((LocationManager) context.getSystemService(Context.LOCATION_SERVICE));
        this.mediatorLiveData.addSource(bugCountLiveData, bugImage -> postInfoIfEnough(bugImage, locationLiveData.getValue()));
        this.mediatorLiveData.addSource(locationLiveData, location -> postInfoIfEnough(bugCountLiveData.getValue(), location));
    }

    public void setBugImage(BugImage bugImage) {
        new BugCalculationTask(bugImage).execute();
    }

    private void postInfoIfEnough(Pair<BugImage, Integer> bugCount, Location location) {
        if (bugCount == null) {
            return;
        }
        if (location == null) {
            return;
        }
        BugCountInformation value = new BugCountInformation(
                UUID.randomUUID().toString(),
                bugCount.first.getName(),
                location.getLatitude(),
                location.getLongitude(),
                bugCount.second,
                System.currentTimeMillis(),
                "");
        mediatorLiveData.setValue(value);
    }

    public LiveData<BugCountInformation> getBugCountLiveData() {
        return mediatorLiveData;
    }

    @SuppressLint("StaticFieldLeak")
    private class BugCalculationTask extends AsyncTask<Void, Void, Void> {

        private final BugImage bugImage;

        private BugCalculationTask(BugImage bugImage) {
            this.bugImage = bugImage;
        }

        @Override
        protected Void doInBackground(Void... bugImages) {
            Context context = contextRef.get();
            if (context != null) {
                BugCount bugCount = new BugCount(context);
                try {
                    int count = bugCount.calculate(bugImage.getUri());
                    bugCountLiveData.postValue(new Pair<>(bugImage, count));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

}
