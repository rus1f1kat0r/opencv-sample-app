package com.example.rus1f1kat0r.testapp.viewmodel;


import android.arch.lifecycle.MutableLiveData;

import com.example.rus1f1kat0r.testapp.model.BugCountInformation;
import com.example.rus1f1kat0r.testapp.service.BugPhotosRepository;

public class BugInfoDetailsLiveData extends MutableLiveData<BugCountInformation> {

    private final BugPhotosRepository repository;
    private final String bugUuid;
    private final BugPhotosRepository.BugInfoListener listener = new BugPhotosRepository.BugInfoListener(this::postValue);

    public BugInfoDetailsLiveData(BugPhotosRepository repository, String bugUuid) {
        this.repository = repository;
        this.bugUuid = bugUuid;
    }

    @Override
    protected void onActive() {
        super.onActive();
        repository.addOnDetailsListener(bugUuid, listener);
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        repository.removeOnDetailsListener(bugUuid, listener);
    }
}
