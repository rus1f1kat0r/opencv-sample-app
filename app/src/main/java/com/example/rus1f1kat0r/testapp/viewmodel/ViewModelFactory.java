package com.example.rus1f1kat0r.testapp.viewmodel;


import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.NonNull;

public class ViewModelFactory extends ViewModelProviders.DefaultFactory {

    private final Application application;

    public ViewModelFactory(Application application) {
        super(application);
        this.application = application;
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(BugCountingViewModel.class)) {
            return (T) new BugCountingViewModel(application);
        }
        return super.create(modelClass);
    }
}
