package com.example.rus1f1kat0r.testapp.model;


public class Location {

    private final double lat;
    private final double lng;

    public Location() {
        this(0, 0);
    }

    public Location(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }


}
