package com.example.rus1f1kat0r.testapp.setup;

import java.util.ArrayList;
import java.util.List;

public class InitializationManager {

    private final List<SetUp> setUps = new ArrayList<>();
    private Runnable onSuccess;

    public InitializationManager() {
    }

    public InitializationManager also(SetUp setUp) {
        setUps.add(setUp);
        return this;
    }

    public InitializationManager doOnSuccess(Runnable success) {
        onSuccess = success;
        return this;
    }

    public static InitializationManager with(SetUp initial) {
        return new InitializationManager().also(initial);
    }

    public void initialize() {
        for (SetUp each : setUps) {
            if (!each.verify()) {
                each.setUp();
                return;
            }
        }
        if (onSuccess != null) {
            onSuccess.run();
        }
    }
}
