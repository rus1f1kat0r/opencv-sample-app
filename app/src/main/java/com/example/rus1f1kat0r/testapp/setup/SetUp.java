package com.example.rus1f1kat0r.testapp.setup;

public interface SetUp {
    void setUp();
    boolean verify();
}
