package com.example.rus1f1kat0r.testapp.ui;


import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rus1f1kat0r.testapp.R;
import com.example.rus1f1kat0r.testapp.databinding.FragmentBugInfoDetailsBinding;
import com.example.rus1f1kat0r.testapp.viewmodel.MapDataViewModel;

public class BugDetailsFragment extends BottomSheetDialogFragment {

    private MapDataViewModel mapDataViewModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentBugInfoDetailsBinding bugInfoBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_bug_info_details, container, false);
        mapDataViewModel = ViewModelProviders.of(getActivity()).get(MapDataViewModel.class);
        mapDataViewModel.getCurrentBugInfo().observe(this, bugInfoBinding::setBugInfo);
        bugInfoBinding.getRoot().findViewById(R.id.save).setOnClickListener(v -> {
            mapDataViewModel.addBugCountInformation(bugInfoBinding.getBugInfo());
        });
        return bugInfoBinding.getRoot();
    }
}
