package com.example.rus1f1kat0r.testapp.model;

import android.net.Uri;
import android.support.annotation.NonNull;

public class BugImage {
    @NonNull
    private final transient Uri uri;
    private final String name;

    public BugImage() {
        this.uri = Uri.EMPTY;
        this.name = "";
    }

    public BugImage(@NonNull Uri uri, String name) {
        this.uri = uri;
        this.name = name;
    }

    @NonNull
    public Uri getUri() {
        return uri;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BugImage bugImage = (BugImage) o;

        if (!uri.equals(bugImage.uri)) return false;
        return name != null ? name.equals(bugImage.name) : bugImage.name == null;
    }

    @Override
    public int hashCode() {
        int result = uri.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
