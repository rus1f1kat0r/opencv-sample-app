package com.example.rus1f1kat0r.testapp.service;

import com.example.rus1f1kat0r.testapp.model.BugCountInformation;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class BugPhotosRepository {

    public static final String PATH_BUGS = "bugs";
    private final DeviceIdentification identification;

    public BugPhotosRepository(DeviceIdentification identification) {
        this.identification = identification;
    }

    public void save(BugCountInformation data) {
        DatabaseReference deviceData = getReferenceForDevice();
        DatabaseReference bug = deviceData.child(PATH_BUGS).child(data.getUuid());
        bug.setValue(data, (databaseError, databaseReference) -> {
            if (databaseError == null) {
                databaseReference.push();
            }
        });
    }

    private DatabaseReference getReferenceForDevice() {
        return FirebaseDatabase.getInstance().getReference(identification.getIdentifier());
    }

    public void addOnDataListener(BugsInfoListener valueEventListener) {
        DatabaseReference deviceData = getReferenceForDevice();
        deviceData.child(PATH_BUGS).addValueEventListener(valueEventListener);
    }

    public void removeOnDataListener(BugsInfoListener valueEventListener) {
        DatabaseReference deviceData = getReferenceForDevice();
        deviceData.child(PATH_BUGS).removeEventListener(valueEventListener);
    }

    public void addOnDetailsListener(String uuid, BugInfoListener listener) {
        getReferenceForDevice().child(PATH_BUGS).child(uuid).addValueEventListener(listener);
    }

    public void removeOnDetailsListener(String uuid, BugInfoListener listener) {
        getReferenceForDevice().child(PATH_BUGS).child(uuid).removeEventListener(listener);
    }

    public interface OnDataAvailable {
        void onData(List<BugCountInformation> all);
    }

    public interface OnBugInfoAvailable {
        void onData(BugCountInformation information);
    }

    public static class BugsInfoListener implements ValueEventListener {
        private final OnDataAvailable listener;

        public BugsInfoListener(OnDataAvailable listener) {
            this.listener = listener;
        }

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            List<BugCountInformation> all = new ArrayList<>();
            for (DataSnapshot bugInfo: dataSnapshot.getChildren()) {
                all.add(bugInfo.getValue(BugCountInformation.class));
            }
            listener.onData(all);
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    }

    public static class BugInfoListener implements  ValueEventListener {
        private final OnBugInfoAvailable listener;

        public BugInfoListener(OnBugInfoAvailable listener) {
            this.listener = listener;
        }

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            listener.onData(dataSnapshot.getValue(BugCountInformation.class));
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    }
}
