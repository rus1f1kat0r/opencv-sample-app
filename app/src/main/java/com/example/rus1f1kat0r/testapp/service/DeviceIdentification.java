package com.example.rus1f1kat0r.testapp.service;

import android.content.Context;
import android.provider.Settings;

import static android.provider.Settings.Secure.ANDROID_ID;

/**
 * Very simple implementation of the device identification. May be replaced with whatever we want.
 */
public class DeviceIdentification {

    private final Context context;

    public DeviceIdentification(Context context) {
        this.context = context;
    }

    public String getIdentifier() {
        return Settings.Secure.getString(context.getContentResolver(), ANDROID_ID);
    }
}
