package com.example.rus1f1kat0r.testapp.setup;


import android.content.Context;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

public class OpenCvInstalled implements SetUp {

    private final Context context;

    private BaseLoaderCallback cvInstalledCallback;

    private static boolean successfullyInstalled;

    public OpenCvInstalled(Context context, Runnable onSuccess) {
        this.context = context;
        this.cvInstalledCallback = new BaseLoaderCallback(context) {
            @Override
            public void onManagerConnected(int status) {
                switch (status) {
                    case LoaderCallbackInterface.SUCCESS: {
                        successfullyInstalled = true;
                        onSuccess.run();
                        break;
                    }
                    default: {
                        super.onManagerConnected(status);
                    }
                    break;
                }
            }
        };
    }

    @Override
    public void setUp() {
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, context, cvInstalledCallback);
    }

    @Override
    public boolean verify() {
        return successfullyInstalled || OpenCVLoader.initDebug();
    }
}
