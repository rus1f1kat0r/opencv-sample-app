package com.example.rus1f1kat0r.testapp.viewmodel;


import android.arch.lifecycle.MutableLiveData;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

class LocationLiveData extends MutableLiveData<Location> {

    private final LocationManager locationManager;
    private final SampleLocationListener locationListener = new SampleLocationListener();

    LocationLiveData(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    @Override
    protected void onActive() {
        super.onActive();
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 5, locationListener);
        } catch (SecurityException e) {

        }
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        locationManager.removeUpdates(locationListener);
    }

    private class SampleLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            setValue(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }
}
